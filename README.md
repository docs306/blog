# 使用

```sh
# 运行
yarn dev 
# or yarn build

```


## 分割线

---

## 容器演示

::: tip 标题

这是是你

```js
const bar = "bar";
```

:::

::: warning 标题

这是是你

```js
const bar = "bar";
```

:::

::: danger 标题

这是是你

```js
const bar = "bar";
```

:::



## 图片演示

![图片描述](./2022-1-11md-test.assets/20220225203015.png)

## 链接演示

[Markdown 语法介绍](https://docs.github.com/en/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)

### 图片链接

图片还可以和链接嵌套使用，能够实现推荐卡片的效果，用法如下：

[![链接和图片嵌套](./2022-1-11md-test.assets/image-20220114095443199.png)](https://artiely.gitee.io)

## 文字修饰

~~这是要被删除的内容。~~

**加粗** *斜体*  *HelloWorld*



### 代码组



:::: code-group
::: code-group-item FOO

```js
const foo = "foo";
```

:::
::: code-group-item BAR

```js
const bar = "bar";
```

:::
::::

## 有序无序
- 早安 被生物钟叫醒的苦命打工人。
- 早上好 在天愿作比翼鸟 在地怨为打工人。
- 这么不努力，怎么做打工人啊你！
- 我要悄悄打工，然后惊艳所有人！
- [ ] 别人的朋友圈都是帅哥美女 我的朋友圈全是打工人。
- [x] 敢上九天揽月 敢下五洋捉鳖 但却不敢迟到 因为迟到扣钱 早安打工人。

1. 早安 被生物钟叫醒的苦命打工人。
1. 早上好 在天愿作比翼鸟 在地怨为打工人。
1. 这么不努力，怎么做打工人啊你！
1. 我要悄悄打工，然后惊艳所有人！
1. [ ] 别人的朋友圈都是帅哥美女 我的朋友圈全是打工人。
1. [x] 敢上九天揽月 敢下五洋捉鳖 但却不敢迟到 因为迟到扣钱 早安打工人。


### 单行

`javascript`

### diff 效果

```diff
+ 新增项
- 删除项
```

## 自定义容器


::: details
这是一个 details 标签
:::


- 示例 2 （自定义标题）：

::: danger STOP
危险区域，禁止通行
:::



::: details 点击查看代码

```js
console.log("你好，VuePress！");
```

:::
