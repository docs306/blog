/**
 * Author: Meng
 * Date: 2023-
 * Desc: 
 */

import { defineUserConfig, defaultTheme } from 'vuepress';
import navbar from './theme/navbar';
import sidebar from './theme/sidebar';

export default defineUserConfig({
  lang: 'zh-CN',
  title: 'Meng',
  // head: ['link', { rel: 'icon', href: '/icon/logo.png' }],
  description: '这是我的第一个 VuePress 站点',
  theme: defaultTheme({
    toggleColorMode: '',
    logo: '/icon/logo.png',
    // home: '/',
    navbar,
    sidebar,
  }),
})