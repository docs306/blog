/**
 * Author: Meng
 * Date: 2023-
 * Desc: 
 */
const navbars = [
  {
    text: '首页',
    link: '/'
  },
  {
    text: '建模',
    children: [
      {
        text: 'Blander',
        children: [
          {
            text: '基础',
            link: '/modeling/blander/',
          },
          {
            text: 'UV',
            link: '/modeling/blander/',
          },
          {
            text: '渲染',
            link: '/modeling/blander/',
          },
          {
            text: '材质',
            link: '/modeling/blander/',
          }
        ],
      },
      {
        text: 'Mayan',
        children: [
          {
            text: '界面',
            link: '/modeling/mayan/',
          },
          {
            text: '操作',
            link: '/modeling/mayan/',
          }
        ],
      },
    ],
  },
  {
    text: '编程语言',
    children: [
      {
        text: 'C语言',
        link: '/blog/log23/month01/two.md',
      },
      {
        text: 'Java',
        link: '/blog/log23/month01/two.md',
      },
      {
        text: 'Mysql',
        link: '/blog/log23/month01/two.md',
      },
      {
        text: 'JS',
        children: [
          {
            text: 'ES5',
            link: '/coding/es5/',
          },
          {
            text: 'ES6',
            link: '/coding/es6/index.md',
          }
        ],
      },
    ],
  },
  {
    text: 'Android',
    children: [
      {
        text: '基础',
        children: [
          {
            text: '界面',
            link: '/program/android/',
          },
          {
            text: '操作',
            link: '/program/android/',
          }
        ],
      },
      {
        text: '进阶',
        children: [
          {
            text: '自定义View',
            link: '/program/android/',
          },
          {
            text: '外设',
            link: '/program/android/',
          },
          {
            text: '音频',
            link: '/program/android/',
          },
          {
            text: '视频',
            link: '/program/android/',
          }
        ],
      },
    ],
  },
  {
    text: '服务端',
    children: [
      {
        text: 'Java',
        children: [
          {
            text: '基础',
            link: '/blog/log23/month01/one.md',
          },
          {
            text: '设计模式',
            link: '/blog/log23/month01/two.md',
          }
        ],
      },
      {
        text: 'Node',
        link: '/blog/log23/month01/one.md',
      },
    ],
  },
  {
    text: '前端',
    children: [
      {
        text: 'React',
        link: '/blog/log23/month01/two.md',
      },
      {
        text: 'Vue',
        link: '/blog/log23/month01/two.md',
      },
      {
        text: '小程序',
        link: '/blog/log23/month01/two.md',
      },
    ],
  },
  {
    text: 'Blog',
    children: [
      {
        text: '2022年',
        link: '/',
        // 该元素将一直处于激活状态
        // activeMatch: '/',
      },
      {
        text: '2023年',
        link: '/essay/',
        // 该元素在当前路由路径是 /foo/ 开头时激活,支持正则表达式
        // activeMatch: '^/essay/',
      },
    ],
  },
]
export default navbars;