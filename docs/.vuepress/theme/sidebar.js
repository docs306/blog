/**
 * Author: Meng
 * Date: 2023-
 * Desc: 
 */

const sidebars = {
  '/blog/': [
    {
      text: '博客',
      collapsible: true,
      children: [
        { text: 'github', link: 'https://github.com' },
        { text: 'bing', link: 'https://bing.com' },
      ],
    },
    {
      text: '博客2',
      collapsible: true,
      children: [
        { text: 'github', link: 'https://github.com' },
        { text: 'bing', link: 'https://bing.com' },
      ],
    },
  ],
  '/program/android': [
    { text: 'Android', link: '/essay/index.md' },
  ],
  '/program/mini': [
    { text: 'Mini', link: '/essay/index.md' },
  ],
  '/program/react': [
    { text: 'React', link: '/essay/index.md' },
  ],
  '/coding/es6/': [
    { text: 'let', link: '/coding/es6/let.md' },
    { text: 'const', link: '/coding/es6/const.md' },
    { text: '变量的解构赋值', link: '/coding/es6/destructuring.md' },
    { text: '数值的扩展', link: '/coding/es6/number.md' },
    { text: '字符串的扩展', link: '/coding/es6/string.md' },
    { text: '字符串新增方法', link: '/coding/es6/string-methods.md' },
    { text: '正则的扩展', link: '/coding/es6/regex.md' },
    { text: '函数的扩展', link: '/coding/es6/function.md' },
    { text: '数组的扩展', link: '/coding/es6/array.md' },
    { text: '对象的扩展', link: '/coding/es6/object.md' },
    { text: '对象新增的方法', link: '/coding/es6/object-methods.md' },
    { text: 'Symbol', link: '/coding/es6/symbol.md' },
    { text: 'Set与Map', link: '/coding/es6/set-map.md' },
    { text: 'Proxy', link: '/coding/es6/proxy.md' },
    { text: 'Reflect', link: '/coding/es6/reflect.md' },
    { text: 'Promise对象', link: '/coding/es6/promise.md' },
    { text: 'Iterator和循环', link: '/coding/es6/iterator.md' },
    { text: 'Generator函数', link: '/coding/es6/generator.md' },
    { text: 'Generator异步', link: '/coding/es6/generator-async.md' },
    { text: 'Async使用', link: '/coding/es6/async.md' },
    { text: 'Class使用', link: '/coding/es6/class.md' },
    { text: 'Class继承', link: '/coding/es6/class-extends.md' },
    { text: 'Module语法', link: '/coding/es6/module.md' },
    { text: 'Module加载', link: '/coding/es6/module-loader.md' },
    { text: '编程风格', link: '/coding/es6/style.md' },
    { text: 'ECMAScript 规格', link: '/coding/es6/spec.md' },
    { text: '异步迭代器', link: '/coding/es6/async-iterator.md' },
    { text: 'ArrayBuffer', link: '/coding/es6/arraybuffer.md' },
    { text: 'Decorator装饰器', link: '/coding/es6/decorator.md' },
    { text: '函数式编程', link: '/coding/es6/fp.md' },
    { text: 'SIMD', link: '/coding/es6/simd.md' },
    { text: 'Mixin', link: '/coding/es6/mixin.md' },
    { text: '鸣谢', link: '/coding/es6/acknowledgment.md' },
    { text: '参考链接', link: '/coding/es6/reference.md' },
  ],
  '/coding/mysql': [
    { text: 'Mysql', link: '/essay/index.md' },
  ],
  '/essay/': [
    {
      text: '文章',
      children: ['/essay/index.md', '/essay/index.md'],
    },
  ]
}
export default sidebars;